require 'benchmark/ips'
require "excel_fun"

module Formula
  extend self

  def pmt(rate, nper, pv, fv=0, type=0)
    rate = parse_float(rate)
    nper = parse_float(nper)
    pv   = parse_float(pv)
    fv   = parse_float(fv)

    if rate == 0
      result = (pv + fv) / nper;
    else
      term = (1 + rate) ** nper

      if type == 1
        result = (fv * rate / (term - 1) + pv * rate / (1 - 1 / term)) / (1 + rate);
      elsif type == 0
        result = fv * rate / (term - 1) + pv * rate / (1 - 1 / term);
      else
        return Float::NAN
      end
    end

    -result
  end

  private

  def parse_float(n)
    Float(n)
  rescue ArgumentError
    Float::NAN
  end
end

Benchmark.ips do |x|

  x.report("Ruby") do |times|
    i = 0
    while i < times
      Formula.pmt(0.14, 12, -10000, 0, 0)
      i += 1
    end
  end

  x.report("C Ext") do |times|
    i = 0
    while i < times
      ExcelFun.pmt(0.14, 12, -10000, 0, 0)
      i += 1
    end
  end

  x.compare!
end
