#include <ruby.h>
#include <math.h>

static VALUE
pmt(VALUE self, VALUE rate_v, VALUE nper_v, VALUE pv_v, VALUE fv_v, VALUE type_v) {
  double result, term;

  double rate = NUM2DBL(rate_v);
  double nper = NUM2DBL(nper_v);
  double pv   = NUM2DBL(pv_v);
  double fv   = NUM2DBL(fv_v);
  int type    = FIX2INT(type_v);

  if (rate == 0) {
    result =  (pv + fv) / nper;
  } else {
    term = pow((1 + rate), nper);
    if (type == 1) {
      result = (fv * rate / (term - 1) + pv * rate / (1 - 1 / term)) / (1 + rate);
    } else {
      result = fv * rate / (term - 1) + pv * rate / (1 - 1 / term);
    }
  }

  return rb_float_new(-result);
}

void Init_excel_fun(void) {
  VALUE cExcelFun;

  cExcelFun = rb_const_get(rb_cObject, rb_intern("ExcelFun"));
  rb_define_module_function(cExcelFun, "pmt", pmt, 5);
}
