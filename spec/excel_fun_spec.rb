require 'spec_helper'

describe ExcelFun do
  it 'has a version number' do
    expect(ExcelFun::VERSION).not_to be nil
  end

  it 'does something useful' do
    result = ExcelFun.pmt(0.14, 12.0, -10000.0, 0.0, 0.0)
    expect(result.round(2)).to eq 1766.69
  end
end
